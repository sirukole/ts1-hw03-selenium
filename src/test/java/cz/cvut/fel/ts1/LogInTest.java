package cz.cvut.fel.ts1;

import cz.cvut.fel.ts1.pages.AdvanSearch;
import cz.cvut.fel.ts1.pages.ArticlePage;
import cz.cvut.fel.ts1.pages.Home;
import cz.cvut.fel.ts1.pages.ResultOfSearch;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

public class LogInTest {

    static WebDriver driver;
    private final String email_test = "lenakot4088@gmail.com";
    private final String pw_test = "accountForTest1100";
//    private int numberArticle = 1;

    @BeforeAll
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Admin\\prog_cvut\\ts1\\hw\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/data.csv", numLinesToSkip = 1)
    public void goToPageHome_goToLogin_fillOutFealds_Login(int numberArticle, String articleTitle, String articleDate, String articleDOI) {

        driver.get("https://link.springer.com/");
        Home homePage = new Home(driver);

        //prilaseni
//        LogIn logInPage = homePage.openLogIn();
//        logInPage.fillOutRegistrationForm(email_test, pw_test);
//        logInPage.sendForm();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //advanced search
        AdvanSearch advanSearchPage = homePage.openAdvanSearch();

        //advanced search "Page Object Model", "Selenium Testing", "2021"
        advanSearchPage.fillOutSearchInputs("Page Object Model", "Selenium Testing", "2021");

        ResultOfSearch resultOfSearch = advanSearchPage.searchSend();

        resultOfSearch.contentType_Article();
        ArticlePage articlePage = resultOfSearch.getArticleFromResult(numberArticle);

        Assertions.assertEquals(articleTitle, articlePage.getTitle());
        Assertions.assertEquals(articleDate, articlePage.getDatePublished());
        Assertions.assertEquals(articleDOI, articlePage.getDOI());

    }

}
