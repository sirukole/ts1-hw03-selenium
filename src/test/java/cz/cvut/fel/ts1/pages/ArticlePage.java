package cz.cvut.fel.ts1.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ArticlePage {

    private WebDriver driver;

    private By titleInfo = By.cssSelector("#main-content > main > article > div.c-article-header > header > h1");
//solution:
//    #main-content > main > article > div.c-article-header > header > ul.c-article-identifiers > li:nth-child(2) > a > time
//    #main-content > main > article > div.c-article-header > header > ul.c-article-identifiers > li > a > time
//    #article-info-content > div > div > ul > li:nth-child(1) > p > span.c-bibliographic-information__value > time
//    now:
//    #article-info-content > div > div:nth-child(2) > ul.c-bibliographic-information__list > li:nth-child(1) > p > span.c-bibliographic-information__value > time

    private By datePublishedInfo = By.cssSelector("#main-content > main > article > div.c-article-header > header > ul.c-article-identifiers > li > a > time");
    private By DOIInfo = By.cssSelector("#article-info-content > div > div > ul > li.c-bibliographic-information__list-item.c-bibliographic-information__list-item--doi > p > span.c-bibliographic-information__value > a");
//#article-info-content > div > div > ul > li.c-bibliographic-information__list-item.c-bibliographic-information__list-item--doi > p > span.c-bibliographic-information__value > a
    private String title;
    private String datePublished;
    private String DOI;

    public ArticlePage(WebDriver driver) {
        this.driver = driver;
    }

    public ArticlePage saveInfo() {
        WebElement titleElem = driver.findElement(titleInfo);
        WebElement dateElem = driver.findElement(datePublishedInfo);
        WebElement DOIElem = driver.findElement(DOIInfo);

        title = titleElem.getText();
        datePublished = dateElem.getText();
        DOI = DOIElem.getText();
        System.out.println("title: " + title);
        System.out.println("datePublished: " + datePublished);
        System.out.println("DOI: " + DOI);
        return new ArticlePage(driver);
    }

    public String getTitle() {
        return title;
    }

    public String getDatePublished() {
        return datePublished;
    }

    public String getDOI() {
        return DOI;
    }
}
