package cz.cvut.fel.ts1.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LogIn {

    private WebDriver driver;
    protected static String urlLogin = "https://link.springer.com/signup-login";

    private By email_input = By.id("login-box-email");
    private By pw_input = By.id("login-box-pw");
    private By logIn_button = By.cssSelector("#login-box > div > div.form-submit > button:nth-child(1)");

    public LogIn(WebDriver driver) {
        this.driver = driver;
    }


    public void fillOutRegistrationForm(String email, String pw) {
        WebElement email1 = driver.findElement(email_input);
        email1.sendKeys(email);

        WebElement pw1 = driver.findElement(pw_input);
        pw1.sendKeys(pw);
    }

    public LogIn sendForm() {
        WebElement button = driver.findElement(logIn_button);
        button.submit();
        return new LogIn(driver);
    }
}
