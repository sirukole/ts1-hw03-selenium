package cz.cvut.fel.ts1.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Home {

    private static WebDriver driver;

    private By logIn_button = By.cssSelector("#header > div.cross-nav.cross-nav--wide > div.auth.flyout > a");
    private By search_button = By.cssSelector("#header > div.panel-search > form.big-search > div.flyout > button");
    private By advanSearch_button = By.cssSelector("#advanced-search-link");

    public Home(WebDriver driver) {
        this.driver = driver;
    }

    public LogIn openLogIn() {
        driver.findElement(logIn_button).click();
        return new LogIn(driver);
    }

    public AdvanSearch openAdvanSearch() {
        driver.findElement(search_button).click();
        driver.findElement(advanSearch_button).click();
        return new AdvanSearch(driver);
    }
}
