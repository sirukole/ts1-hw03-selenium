package cz.cvut.fel.ts1.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class AdvanSearch {

    private WebDriver driver;
    private By allWords = By.cssSelector("#all-words");
    private By leastWords = By.cssSelector("#least-words");
    private By dataType_select = By.cssSelector("#date-facet-mode");
    private By startYear = By.cssSelector("#facet-start-year");
    private By searchButton = By.cssSelector("#submit-advanced-search");

    public AdvanSearch(WebDriver driver) {
        this.driver = driver;
    }

    public void fillOutSearchInputs(String allW, String leastW, String year) {

        WebElement words = driver.findElement(allWords);
        WebElement lWords = driver.findElement(leastWords);

        words.sendKeys(allW);
        lWords.sendKeys(leastW);

        Select dateSelect = new Select(driver.findElement(dataType_select));
        dateSelect.selectByValue("in");

        WebElement years = driver.findElement(startYear);

        years.sendKeys(year);
    }

    public ResultOfSearch searchSend() {
        WebElement butt = driver.findElement(searchButton);
        butt.submit();
        return new ResultOfSearch(driver);
    }
}
