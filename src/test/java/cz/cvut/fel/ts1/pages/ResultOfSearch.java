package cz.cvut.fel.ts1.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ResultOfSearch {

    private WebDriver driver;
    private By articles = By.cssSelector("#content-type-facet > ol > li:nth-child(2) > a");
    private By chooseArticle1 = By.cssSelector("#content-type-facet > ol > li:nth-child(2) > a");
    private By chooseArticle = By.cssSelector("#content-type-facet > ol:nth-child(2) > li:nth-child(2) > a:nth-child(1)");

    private By firstArticle = By.cssSelector("#results-list > li:nth-child(1) > h2 > a");
    private By secondArticle= By.cssSelector("#results-list > li:nth-child(2) > h2 > a");
    private By thirdArticle = By.cssSelector("#results-list > li:nth-child(3) > h2 > a");
    private By fourthArticle = By.cssSelector("#results-list > li:nth-child(4) > h2 > a");

    public ResultOfSearch(WebDriver driver) {
        this.driver = driver;
    }

    public ResultOfSearch contentType_Article(){
        WebElement artic = driver.findElement(chooseArticle);
        artic.click();
        return new ResultOfSearch(driver);
    }

    public ArticlePage getArticleFromResult(int numberArticle){
        ArticlePage page = new ArticlePage(driver);
        if (numberArticle == 1) {
            driver.findElement(firstArticle).click();
            page.saveInfo();
        } else if (numberArticle == 2) {
            driver.findElement(secondArticle).click();
            page.saveInfo();
        } else if (numberArticle == 3) {
            driver.findElement(thirdArticle).click();
            page.saveInfo();
        } else if (numberArticle == 4) {
            driver.findElement(fourthArticle).click();
            page.saveInfo();
        } else {
            page = null;
        }
        return page;
    }
}
